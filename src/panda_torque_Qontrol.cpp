// Copyright (c) 2017 Franka Emika GmbH0
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <panda_qontrol/panda_torque_Qontrol.h>
#include <cmath>
#include <memory>
#include <chrono> 
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

using namespace std;
using namespace Qontrol;

namespace panda_qontrol{


bool TorqueController::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {    
    this->node_handle = node_handle;
    auto ns = this->node_handle.getNamespace();
    parent_ns = ns.substr(0, ns.find('/',1)) ;
    parent_node_handle = ros::NodeHandle(parent_ns);
    std::cout << "Parent namespace : " << parent_ns << std::endl;
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    string arm_id;
    if (!node_handle.getParam("arm_id", arm_id)) {
        ROS_ERROR_STREAM("Could not read parameter arm_id");
        return false;
    }   

    std::vector<std::string> joint_names;
    if (!node_handle.getParam("joint_names", joint_names)) {
        ROS_ERROR("Could not parse joint names");
    }
    if (joint_names.size() != 7) {
        ROS_ERROR_STREAM("Wrong number of joint names, got "
        << joint_names.size() << " instead of 7 names!");
        return false;
    }
   
    auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
    if (state_interface == nullptr) {
        ROS_ERROR("Could not get state interface from hardware");
        return false;
    }
    try {
        state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>( state_interface->getHandle(arm_id+"_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "Exception getting state handle: " << e.what());
            return false;
    }

    auto* effort_joint_interface = robot_hardware->get<hardware_interface::EffortJointInterface>();
    if (effort_joint_interface == nullptr)
    {
        ROS_ERROR_STREAM(
            "Error getting effort joint interface from hardware");
        return false;
    }
    for (size_t i = 0; i < 7; ++i)
    {
        try 
        {
            joint_handles_.push_back(effort_joint_interface->getHandle(joint_names[i]));
        }
        catch (const hardware_interface::HardwareInterfaceException& ex)
        {
            ROS_ERROR_STREAM(
            "Exception getting joint handles: " << ex.what());
            return false;
        }
    }

    pose_curr_publisher.init(parent_node_handle, "current_cartesian_pose", 1);
    vel_curr_publisher.init(parent_node_handle, "current_cartesian_velocity", 1);
    
    return true;
}

void TorqueController::starting(const ros::Time&)
{
    // ROS_WARN_STREAM("Starting QP Controller on the real Panda");
    getRosParam("/is_sim", is_sim);
    std::string robot_description;
    getRosParam(parent_ns+"/robot_description", robot_description);
    std::vector<std::string> joint_names;
    ros::param::get(node_handle.getNamespace()+"/joint_names", joint_names);
    p_gains.resize(6);
    getRosParam(node_handle.getNamespace()+"/p_gains", p_gains);
    d_gains.resize(6);
    getRosParam(node_handle.getNamespace()+"/d_gains", d_gains);
    dtorque_max.resize(7);
    getRosParam(node_handle.getNamespace()+"/dtorque_max", dtorque_max);
    kp_reg.resize(7);
    getRosParam(node_handle.getNamespace()+"/kp_reg", kp_reg);
    std::string controlled_frame;
    ros::param::get(node_handle.getNamespace()+"/controlled_frame", controlled_frame);
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());

    qontrol_model =
      Model::RobotModel<Model::RobotModelImplType::PINOCCHIO>::loadModelFromString(robot_description,"panda_link0",controlled_frame);
    torque_problem = std::make_shared<Problem<Qontrol::ControlOutput::JointTorque>> (qontrol_model);

    main_task = torque_problem->task_set->add<Task::CartesianAcceleration>("MainTask"); // <-- Based on the template given will implement correct task representation  
    regularisation_task = torque_problem->task_set->add<Task::JointTorque>("RegularisationTask",1e-5); // <-- Based on the template given will implement correct task representationn

        
    auto joint_configuration_constraint = torque_problem->constraint_set->add<Constraint::JointConfiguration>("JointConfigurationConstraint");
    joint_configuration_constraint->setHorizon(15);
    auto joint_velocity_constraint = torque_problem->constraint_set->add<Constraint::JointVelocity>("JointVelocityConstraint");
    joint_velocity_constraint->setHorizon(15);
    auto joint_torque_constraint = torque_problem->constraint_set->add<Constraint::JointTorque>("JointTorqueConstraint");
   
    if (!initialized) // franka_control starts twice but the first one doesn't initialize the torque_qp ...
    {
        trajectory.interface->init(parent_node_handle,"robot_description",q_init,joint_names);
        initialized = true; 
    }
}


void TorqueController::update(const ros::Time&, const ros::Duration& period) {    

    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    // get state variables
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q(robot_state.q.data());
    Eigen::Matrix<double,7,1>  qd(robot_state.dq.data());
    Qontrol::RobotState robot_state_;
    robot_state_.joint_position = q;
    robot_state_.joint_velocity = qd;
    qontrol_model->setRobotState(robot_state_);

    // Update model
      // First calls the forwardKinematics on the model, then computes the placement of each frame.
   
    // pinocchio::Motion xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(torque_qp.getControlledFrame()),pinocchio::ReferenceFrame::LOCAL);
    Eigen::Matrix<double,6,1> xd = qontrol_model->getFrameVelocity(qontrol_model->getTipFrameName());
    //Update the trajectory
    trajectory.interface->updateTrajectory();
    pinocchio::SE3 oMdes(trajectory.interface->getCartesianPose().matrix());
    pinocchio::SE3 current_pose(qontrol_model->getFramePose(qontrol_model->getTipFrameName()).matrix());
    // Compute error
    const pinocchio::SE3 tipMdes = current_pose.actInv(oMdes);
    Eigen::Matrix<double,6,1> err =  pinocchio::log6(tipMdes).toVector();
    
    // Proportional controller with feedforward
    // Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + (2.0 *p_gains.cwiseSqrt()).cwiseProduct( trajectory.interface->getCartesianVelocity() - xd.toVector() ) + trajectory.interface->getCartesianAcceleration();
    Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + d_gains.cwiseProduct(  - xd ) + trajectory.interface->getCartesianAcceleration();

    main_task->setTargetAcceleration(xdd_star);
    regularisation_task->setTargetTorque(qontrol_model->getJointGravityTorques() - robot_state_.joint_velocity);
    regularisation_task->setWeightingMatrix(qontrol_model->getInverseJointInertiaMatrix());
    torque_problem->update(period.toSec());
    if (torque_problem->solutionFound())
    {
        for (size_t i = 0; i < 7; ++i)
        {
            if (is_sim){
                joint_handles_[i].setCommand(torque_problem->getPrimalSolution()[i]);
            }
            else{
                joint_handles_[i].setCommand(torque_problem->getPrimalSolution()[i] - qontrol_model->getJointGravityTorques()[i]);
            }
        }      
    }

    publishCartesianState();
    
}
void TorqueController::publishCartesianState()
{
    Eigen::Affine3d current_pose(qontrol_model->getFramePose(qontrol_model->getTipFrameName()).matrix());
    tf::poseEigenToMsg(current_pose,cartesian_pose_msg);
    pinocchio::Motion xd(qontrol_model->getFrameVelocity(qontrol_model->getTipFrameName()).matrix());
    tf::twistEigenToMsg(xd.toVector(), cartesian_velocity_msg);
    //Publish robot desired pose
    if (pose_curr_publisher.trylock())
    {
        pose_curr_publisher.msg_.header.stamp = ros::Time::now();
        pose_curr_publisher.msg_.header.frame_id = "world";
        pose_curr_publisher.msg_.pose = cartesian_pose_msg;
        pose_curr_publisher.unlockAndPublish();
    }
    //Publish robot desired velocity
    if (vel_curr_publisher.trylock())
    {
        vel_curr_publisher.msg_.header.stamp = ros::Time::now();
        vel_curr_publisher.msg_.header.frame_id = "world";
        vel_curr_publisher.msg_.twist = cartesian_velocity_msg;
        vel_curr_publisher.unlockAndPublish();
    }
}
}  // namespace panda_qontrol


PLUGINLIB_EXPORT_CLASS(panda_qontrol::TorqueController,
controller_interface::ControllerBase)
