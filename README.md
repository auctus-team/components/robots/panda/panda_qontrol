<!-- [![pipeline status](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control) -->

# Panda Qontrol

A ROS package to control a Franka Emika panda robot using the Qontrol library.

<!-- ## Installation

Procedure to install this package along the other necessary ros packages inside a catkin workspace is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_ws)
 -->
